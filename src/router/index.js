import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: { name: 'Login' },
    component:{}
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')/* webpackChunkName: "Login"(按需加載)) */
  },

  {
    path:'/Search',
    name:'Search',
    component:()=>import('../views/Search.vue')   
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
